\documentclass{shortnotes}

%%
\usepackage{style-shortnotes}
%%
\usepackage{custommath_gen}
\usepackage{category-names}
\usepackage{custommath_font}
\usepackage{custommath_tan}

\usepackage{gitinfo2}
\renewcommand{\qlb}{\Lambda}
%%
\title{\normalsize{Tannakian formalism and semi-simplicity of the Satake category}}
\date{}
\author{\normalsize{Aaron Wild}\footnote{\url{aaron.wild@posteo.net}. This is version \gitAbbrevHash{} of this document. An up-to-date version can be found at \url{aaronwild.gitlab.io}}}
\abstracttext{Following Richarz (\cite{og-paper}), we sketch his proof that the graded cohomology functor for the category of $\plg G$-equivariant perverse sheaves on the Grassmanian is an exact and faithful functor. This, together with properties of the convolution product on $\pvs*(\grs_G)$ implies that the latter category is neutral Tannakian, by a theorem of Deligne and Milne. These arguments do not rely on the notion of ULA-sheaves, and the proof is similar to \cite[§7]{mv-sat} (where they use the analytic topology). These notes were prepared for the Kleine AG on the Geometric Satake Equivalence, which took place in Bonn in March 2023. I would like to thank Louis Jaburi for helping me prepare for the talk. Some claims and questions I could not resolve are \todocol{marked accordingly}.}
\bibliography{lit.bib}
\begin{document}
\maketitlepage
\thispagestyle{empty}
\section{Tannakian categories}
\subsection*{Reconstructing a finite group from its category of representation}

\begin{numtext}
  Let $G$ be a finite group, and $k$ a field.
  Then its category of finite-dimensional $k$-linear representations $\repfin_k(G)$ admits a forgetful functor $F$ to the category of $k$-vector spaces.
  Let $\aut^\tensor(F)$ be the group of natural isomorphisms $\lambda\mc F \to F$ that are compatible with the $\tensor$-structure of representations, i.e. $\lambda_{V\tensor W} = \lambda_V \tensor \lambda_W$.
  For every $g\in G$, the $G$-action induces such a $\tensor$-equivariant natural transformation.
  It can be shown that this assignement induces an equivalence of categories
  \[
    \rho \mc G \isomorphism \aut^\tensor(F).
  \]
  Note however that the functor $F$ is crucial, as every two finite groups with same number of conjucagy classes have equivalent categories of finite dimensional representations.
\end{numtext}

\begin{rem}
  A similar statement holds true for algebraic groups over fields, see for example \cite[Thm.9.2]{milne-algebraic}.
\end{rem}

\subsection*{Monoidal categories}
\begin{defn}
  Let $\ccat$ be a category. 
  A \emph{nonunital monoidal structure} on $\ccat$ consists of the following data:
  \begin{itemize}
    \item 
      A functor $\tensor \mc \ccat \times \ccat \to \ccat$, called the \emph{tensor product functor};
    \item 
      A collection of isomorphisms 
      \[
        \phi_{X,Y,Z}
        \mc 
        X \tensor (Y \tensor Z)
        \isomorphism 
        (X\tensor Y) \tensor Z 
      \]
      called the \emph{associativity constraint}, that are functorial in the following sense:
      For every triple of morphisms $f\mc X \to X'$, $g\mc Y \to Y'$, and $h \mc Z \to Z'$, the diagram 
      \[
        \begin{tikzcd}
          X\tensor (Y \tensor Z)
          \ar{r}[above]{\phi}[below]{\sim}
          \ar{d}[left]{f\tensor (g\tensor h)}
          &
          (X \tensor Y) \tensor Z 
          \ar{d}[right]{(f\tensor g) \tensor h}
          \\ 
          X'\tensor(Y'\tensor Z')
          \ar{r}[above]{\sim}[below]{\phi}
          &
          (X'\tensor Y')\tensor Z'
        \end{tikzcd}
      \]
  \end{itemize}
  The isomorphisms $\phi_{X,Y,Z}$ are required to satisfy the following compatibility condition:
  For every quadruple of objects $X,Y,Z,T\in \ccat$, the diagram of isomorphism 
\begin{center}
\begin{tikzpicture}[commutative diagrams/every diagram]
\node (P0) at (90:2.3cm) {$X\otimes (Y\otimes (Z\otimes T))$};
\node (P1) at (90+72:2cm) {$X\otimes ((Y\otimes Z)\otimes T))$} ;
\node (P2) at (90+2*72:2cm) {\makebox[5ex][r]{$(X\otimes (Y\otimes Z))\otimes T$}};
\node (P3) at (90+3*72:2cm) {\makebox[5ex][l]{$((X\otimes Y)\otimes Z)\otimes T$}};
\node (P4) at (90+4*72:2cm) {$(X\otimes Y)\otimes (Z\otimes T)$};
\path[commutative diagrams/.cd, every arrow, every label]
(P0) edge node[swap] {$\id \otimes\phi$} (P1)
(P1) edge node[swap] {$\phi$} (P2)
(P2) edge node {$\phi\otimes \id$} (P3)
(P4) edge node {$\phi$} (P3)
(P0) edge node {$\phi$} (P4);
\end{tikzpicture}
\end{center}
commutes.
\end{defn}

\begin{defn}
  Let $(\ccat,\tensor, \phi)$ be a nonunital monoidal category.
  A \emph{commutativity constraint} on $\ccat$ is a collection of functorial isomorphisms 
  \[
    \psi_{X,Y}
    \mc 
    X \tensor Y 
    \isomorphism 
    Y \tensor X
  \]
  such that for all objects $X,Y$ of $\ccat$, the composition 
  \[
    \begin{tikzcd}
      X \tensor Y  
      \ar{r}[above]{\sim}[below]{\psi_{X,Y}}
      &
      Y \tensor X 
      \ar{r}[above]{\sim}[below]{\psi_{Y,X}}
      &
      X \tensor Y
    \end{tikzcd}
  \]
  equals the identity.
  The transformations $\phi$ and $\psi$ are said to be \emph{compatible} if for all objects $X,Y,Z$ in $\ccat$, the diagram 
  \[
    \begin{tikzcd}
      X\tensor  (Y \tensor Z )
      \ar{r}[above]{\phi}
      \ar{d}[left]{\id \tensor \psi}
      &
      (X \tensor Y)  \tensor Z
      \ar{r}[above]{\psi}
      &
      Z \tensor (X \tensor Y)
      \ar{d}[right]{\phi}
      \\ 
      X \tensor (Z \tensor Y)
      \ar{r}[below]{\phi}
      &
      (X \tensor Z ) \tensor Y
      \ar{r}[below]{\psi \tensor \id}
      &
      (Z \tensor X) \tensor Y
    \end{tikzcd}
  \]
  commutes.\footnote{This can also be arranged as a hexagon.}
  The collection $(\ccat, \tensor, \phi, \psi)$ is called a \emph{nonunital symmetric monoidal category}.
\end{defn}

\begin{defn}
 Let $(\ccat, \tensor, \phi, \psi)$ be a nonunital symmetric monoidal category.
  A pair $(\idob,u)$ where $\idob$ is an object of $\ccat$, $u \mc \idob \isomorphism \idob \tensor \idob $ is an isomorphism is called a \emph{unit object} of $\ccat$ if the functor $X \mapsto \idob \tensor X$ is an equivalence of categories.\footnote{For not-necessarily symmetric monoidal categories, one can also define unitality, but then one has to require left- and right-unitality separately.}
  If $\ccat$ admits a unit object, then it is called a \emph{tensor category}.
\end{defn}

\begin{rem}
  In a symmetric monoidal category, it is possible to ``iterate'' tensor products.
  In these notes, will be oblivious to this subtelty.
\end{rem}

\begin{defn}
  Let $(\ccat,\tensor)$ be a tensor category, and $X,Y\in \ccat$ two objects.
  If the functor $\ccat^{\mathrm{op}} \to \setcat,~T \mapsto \hom_{\ccat}(T\tensor X, Y)$ is representable, we denote representing object by $\ihom(X,Y)$.
\end{defn}

\begin{numtext}
  Let $(\ccat,\tensor)$ be a tensor category, with unit object $\idob$.
  Assume that $\ihom(X,Y)$ exists for all $X,Y\in \ccat$.
  There is a canonical map 
  \[
    \ev_{X,Y} \mc \ihom(X,Y) \tensor X \to Y 
  \]
  corresponding to the identity of $\ihom(X,Y)$.\footnote{For example, in the category of $R$-modules, we have $\ihom(M,N) = \hom_R(M,N)$, and the evaluation map $\ev_{M,N} \mc \hom_R(M,N) \tensor M \to N$ is given by $(f,x) \mapsto f(x)$.}
  These maps induce \emph{composition morphisms}
  \[
    \ihom(X,Y) 
    \tensor 
    \ihom(Y,Z) 
    \to 
    \ihom(X,Z)
  \]
  and isormphisms 
  \[
    \ihom(Z,\ihom(X,Y))
    \isomorphism 
    \ihom(Z\tensor X,Y)
  \]
  The \emph{dual} of an object $X$ is defined as $X^\vee \mc \ihom(X,\idob)$.
  It is possible to extend $(-)^\vee$ to a functor $\ccat^\mathrm{op} \to \ccat$.
  For every $X\in \ccat$, there is a canonical map $X\to X^{\vee\vee}$.
  We say that $X$ is \emph{reflexive} if this map is an isomorphism.
  Moreover, there are, for all finite families of objects $(X_i)$ and $(Y_i)$, canonical morphisms 
  \[
    \bigoplus_I \ihom(X_i,Y_i)
    \to 
    \ihom(\bigoplus_I X_i, \bigoplus_I Y_i).
  \]
\end{numtext}
\begin{defn}
  Let $(\ccat,\tensor)$ be a tensor category. 
  We say that it is \emph{rigid} if:
  \begin{enumerate}
    \item 
      $\ihom(X,Y)$ exists for all objects $X,Y\in \ccat$,
    \item 
      the canonical morphism
      \[
        \ihom(X_1,Y_1) \tensor \ihom(X_2,Y_2)
        \to 
        \ihom(X_1 \tensor X_2, Y_1 \tensor Y_2)
      \]
      is an isomorphism for all collections $X_1,X_2,Y_1,Y_2\in \ccat$, and 
    \item 
      every object of $\ccat$ is reflexive.
  \end{enumerate}
\end{defn}
\subsection*{Tannakian reconstruction}
\begin{prop}[{\cite[Proposition 1.20]{delmil}}]
  \label{prop:tannaka-reconst}
  Let $\ccat$ be a $k$-linear abelian category, where $k$ is a field, and $\tensor\mc \ccat \times \ccat \to \ccat$ a $k$-bilinear functor.
  Suppose there are given:
  \begin{itemize}
    \item 
      A faithful exact $k$-linear functor $F\mc \ccat \to \vec_k$;
    \item
      Functorial isomorphisms 
      \[
        \phi_{X,Y,Z}
        \mc 
        X \tensor (Y \tensor Z) 
        \isomorphism
        (X \tensor Y) \tensor Z,
        ~\text{and}~
        \psi 
        \mc 
        X\tensor Y 
        \isomorphism 
        Y\tensor X.
      \]
    \end{itemize}
    Assume these satisfy the following properties:
    \begin{enumerate}
      \item 
        The diagram 
        \[
          \begin{tikzcd}
            \ccat \times \ccat 
            \ar{r}[above]{\tensor_\ccat}
            \ar{d}[left]{F \times F}
            &
            \ccat 
            \ar{d}[right]{F}
            \\
            \vec_k \times \vec_k
            \ar{r}[below]{\tensor_k}
            &
            \vec_k 
          \end{tikzcd}
        \]
        commutes;
      \item 
        $F(\phi_{X,Y,Z})$ is the usual associativity in $\vec_k$;
      \item 
        $F(\psi_{X,Y})$ is the usual commutativity isomorphism in $\vec_k$;
      \item 
        There exists an object $U$ in $\ccat$, such that $F(U) = k$, and for all $X\in \ccat$, the canonical isormphisms from (i) 
        \[
          F(X) \tensor_k F(U)
          \cong 
          F(X) 
          \cong 
          F(U) \tensor_k F(X)
        \]
        are induced by isormphisms in $\ccat$.\footnote{This is the version of this condition as in \cite[Theorem 9.24]{milne-algebraic}, leaving no room for interpretation what exactly an ``invertible object'' is supposed to be in the setting where $\ccat$ is not already a monoidal category (In the original \cite{delmil}, this condition does not appear, but in a later revised version (\cite{delmilnew}), the object $U$ is required to be ``invertible'', and an example is provided that this is necessary).}
      \item 
        If $L \in \ccat$ is an object such that $F(L)$ has dimension 1, then there exists an object $L'$ in $\ccat$ such that the natural map $L \tensor L' \to \idob$ is an isomorphism.
    \end{enumerate}
    Then $(\ccat,\tensor,\phi,\psi)$ is a rigid abelian tensor category.
\end{prop}

\begin{defn}
  Let $(\ccat,\tensor)$ and $(\ccat',\tensor')$ be tensor categories.
  \begin{enumerate} 
    \item 
      A \emph{tensor fuctor} is a pair $(F,c)$ where $F\mc \ccat \to \ccat'$ is a functor and $c$ is a natural isomorphism 
      \[
        F(X) \tensor' F(Y) 
        \isomorphism 
        F(X\tensor Y),
      \]
      such that $F$ is compatible with the associativity and commutativity constraints and maps an identity object to an identity object.
    \item 
      Let $(F,c)$ and $(D,d)$ be tensor functors.
      A morphism of tensor functors is a natural transformation $\lambda \mc F \to G$ that is compatible with $c$ and $d$ and the isomorphism for the unit.\footnote{If $C,C'$ are both rigid, then every morphism of tensor functors is an isomorphism of tensor functors}
    \item 
      Let $k$ be a field.
      For any $k$-algebra, there is the canonical base-change functor $\phi_R \mc V \mapsto V \tensor_k R$.
      If $(F,c)$ and $(G,d)$ are tensor functors $\ccat \to \vec_k$, then we define the functor $\ihom^\tensor(F,G)$ to be the functor of $k$-algeras 
      \[
        R 
        \mapsto 
        \hom^\tensor(\phi_R \circ F, \phi_R \circ G).
      \]
  \end{enumerate}
\end{defn}




\begin{theorem}[{\cite[Theorem 2.11]{delmil}}]
  \label{thm:reconst}
  Let $(\ccat,\tensor)$ be a rigid abelian tensor category such that $k = \Endo(\idob)$, and let $F\mc \ccat \to \vec_k$ be an exact faithful $k$-linear tensor functor.
  Then:
  \begin{enumerate}
    \item 
      The functor $\iaut*(F)$ is representend by an affine group scheme $G_\ccat$;\footnote{More precisely, a Hopf algebra (whose spectrum will give the affine group scheme).}
    \item 
      The functor $\ccat \to \repcat_k(G_\ccat)$ defined by $F$ is an equivalence of tensor categories.
  \end{enumerate}
\end{theorem} 

\begin{rem}
  The last part of the theorem is crucial, as it asserts not only that the categories $\ccat$ and $\repcat_k(G_\ccat)$ are isomorphic, but also that this isomorphism is compatible with the tensor structures on both sides.
\end{rem}

\subsection*{Properties of $G$ are encoded in $\repcat_k(G)$}

\begin{prop}[{\cite[Prop.2.20]{delmil}}]
  Let $G$ be an affine group scheme over $k$.
  \begin{enumerate}
    \item 
      $G$ is is finite if and only if there exists an object $X$ of $\repcat_k(G)$ such that every object of $\repcat_k(G)$ is isomorphic to a subquotient of $X^n$ for some $n \geq 0$.
    \item 
      $G$ is algebraic if and only if $\repcat_k(G)$ has a tensor generator $X$.
  \end{enumerate}
\end{prop}

The category of (i) is denoted by $\genby{X}$.
A \emph{tensor generator} is an object $X \in \repcat_k(G)$ such that every object of $\repcat_k(G)$ is isomorphic to a subquotient of $P(X,X^\vee)$ for some (not-necessarily fixed) polynomial $P\in \nn[s,t]$.

\begin{prop}[{\cite[Cor.2.22]{delmil}}]
  Assume that $k$ has characteristic zero.
  Then $G$ is connected if and only if, for every representation $X$ of $G$ on which $G$ acts non-trivially, $\genby{X}$ is not stable under $\tensor$.
\end{prop}

\begin{prop}[{\cite[Prop.2.23]{delmil}}]
  Let $G$ be a connected afine group scheme over a field $k$ of characteristic zero.
  The category $\repcat_k(G)$ is semi-simple if and only if $G$ is pro-reductive.
\end{prop}

\section{Schubert cells and varieties}

\begin{defn}
  Let $\mu \in X_+^\vee$ be a reprenstative of a $\plg G$-orbit on $\grs_G$, with $\oo_\mu$ the corresponding orbit, and $j_\mu \mc \oo_\mu \hookrightarrow \olo_\mu$ the open embedding into its reduced closure.
  The scheme $\oo_\mu$ is called a \emph{Schubert cell}, and $\olo_\mu$ is called a \emph{Schubert variety}.
\end{defn}

\begin{rem}
  Even though the Schubert cell $\oo_\mu$ is sometimes called an ``open'' Schubert cell, it is in general not open in $\grs_G$.
\end{rem}

\begin{lem}[{c.f. \cite[Prop.1.3.2]{savior}}]
  \label{lem:orbit-geom}
  We have a stratification
  \[
    \grs_G 
    = 
    \bigsqcup_{\lambda \in X_\ast(T)^+}
    \oo_{\lambda}
  \]
  Moreover, this decomposition is a stratification of $\grs_G$ and, for any $\mu \in X_\ast(T)^+$, it holds  that 
  \[
    \olo_\mu 
    = 
    \bigsqcup_{\substack{\lambda \in X_\ast(T)^+\\ \lambda \leq \mu}}
    \oo_\mu.
  \]
  The Schubert cells $\oo_\mu$ are smooth quasi-projective varieties of dimension $(2\rho,\mu)$, and the Schubert varieties $\olo_\mu$ are projective varieties.
\end{lem}

\begin{theorem}[{\cite[Thm.8]{faltings}}]
  For $\mu \in X_+^\vee$, the scheme $\olo_\mu$ is normal, Cohen-Macaulay and has rational singularities.
\end{theorem}

\section{Semisimplicity of $\pvs*(\grs_G)$ and simple objects}

\begin{numtext}
  Let $X$ be a scheme, and $j \mc Z \hookrightarrow X$ a locally closed immersion.
  Associated to it is the \emph{intermediate extension functor} $j_{!\ast}\mc \pvs(Z) \to \pvs(X)$, which is defined for a complex $C \in \pvs(Z)$ as
  \[
    j_{!\ast}
    C
    = 
    \im(\hhp{0}(j_! C) \to \hhp{0} (j_\ast K)),
  \]
  where $\hhp{i}\defo \pvsp{\tau_{\geq i}}$ is the $i$-th truncation functor for the perverse $t$-structure on $\detcat*(X,\qlb)$.
  These functors allow us to classify the simple objects of $\pvs(X)$:
  They are given by the \emph{intersection complexes}
  \[
    \ic(Z,\shl) \defo j_{!\ast} \shl[d],
  \]
  where $j \mc Z \hookrightarrow X$ is a locally closed embedding, $Z$ is smooth of dimension $d$ and $\shl$ is a local system on $Z$ (\cite[Thm.4.3.1]{bdd}).
  This extends to categories of equivariant perverse sheaves, by considering them as perverse sheaves on the corresponding classifying stack.
  The intersection complex $\shf \defo \ic(Z,\shl)$ satisfies the following properties:
  \begin{equation}
    \label{eq:pvs-basic}
    \restrict{\shf}{X\setminus \overline{Z}} = 0,~~
    \restrict{\shf}{Z} = \shl[\dim Z],~~
    i^\ast \shf \in \pvsp{\detcat^{\leq -1}}(\overline{Z} \setminus Z, \qlb),~~
    i^! \shf \in \pvsp{\detcat^{\geq 1}}(\overline{Z} \setminus Z, \qlb),
  \end{equation}
  where $i \mc \overline{Z} \setminus Z \hookrightarrow X$ is the inclusion.
  \par 
  In the case of the action of a connected group $H$ on a scheme $X$, we can always consider the stratification of $X$ by the orbits of $H$.
  Then the above statement remain valid, if ``locally closed subscheme'' gets replaced by ``orbit''.
  Moreover, even though the category of constructible sheaves with respect to a given stratification is usually not stable under the six operations, this is valid for the orbit stratification.\footnote{I could not find any reference that gives this as a precise statement, so if you do know one, \emph{please} do get in touch!}
  We do not go trough the details of this, or define equivariant perverse sheaves (which is what Richarz is using in its paper).
  So by $\pvs*(\grs_G)$ we mean perverse sheaves with respect to the orbit stratification.
  It will then follow from this (``non-equivariant'') category of perverse sheaves being semi-simple that it is actually isomorphic to the category of $\plg G$-equivariant sheaves (I'm not going to spell this out, more details can be found in \cite{savior}).
\end{numtext}

\begin{prop}
  \label{prop:simple}
  The simple objects in $\pvs*(\grs_G)$ are given by the intersection complexes 
  \[
    \ic_\mu 
    = 
    j_{!\ast}^\mu \qlb[\dim(\oo_\mu)],
  \]
  for $\mu \in X_+^\vee$.
\end{prop}

\begin{proof}
  In a first step, we show that every finite \'etale covering of $\oo_\mu$ necessarily splits, using the geometry of the orbits from \cref{lem:orbit-geom}:
  We can use the Zariski-Nagata purity theorem to relate the \'etale fundamental group of $\olo_\mu$ with the \'etale fundamental group of $\oo_\mu$:
  \begin{inthm}[{\cite[Exp.X,Cor.3.3]{sga1}}]
    Let $X$ be a separated noetherian regular scheme and $j\mc U \hookrightarrow X$ an open subscheme such that every irreducible component of $X\setminus U$ has codimension $\geq 2$.
    Then restriction to $U$ is an equivalence of categories 
    \[
      j^\ast 
      \mc 
      \text{\'etale coverings of $X$}
      \isomorphism 
      \text{\'etale coverings of $U$}.
    \]
    In particular, there is an isomorphism $\pet(U,\overline{x}) \isomorphism \pet(X,\overline{x})$ for every base point $\overline{x}$.
  \end{inthm}
  So we have $\pet(\oo_\mu) = \pet(\olo_\mu)$, and the latter group is trivial (this holds for all normal projective rational varieties over an algebraically closed field, c.f. \cite[Exp.XI,Cor.1.2]{sga1}).
  \par
  \todocol{As the stabilizers of the $\plg G$-action are connected, the triviality of the \'etale fundamental group of $\oo_\mu$ implies} that any $\plg G$-equivariant local system on $\oo_\mu$ is isomorphic to the constant sheaf $\qlb$.\footnote{\todocol{Let $H$ be a connected algebraic group, acting on a scheme $X$. Then the forgetful functor $\locsys_G(X) \to \locsys(X)$ is fully faithful, with essential image given by those $F$ that satisfy $\mathrm{act}^\ast F \cong \mathrm{pr}_2^\ast F$. Now $\plg G$ is connected, shouldn't this suffice?}}
  By the correspondence we recalled above, this gives the description of the simple objects in $\pvs*(\grs_G)$.
\end{proof}

Recall that an abelian category is called \emph{semisimple} if every object can be written as a finite direct sum of simple objects.
\begin{prop}
  \label{prop:semisimple}
  The category $\pvs*(\grs_G)$ is semisimple.\footnote{There are slight differences in the proof of this proposition that appears in the published version and the one in the latest (at the time of writing) arXiv-preprint. We follow the published proof, with additions as in \cite[§1.4.3]{savior}.}
\end{prop}
\begin{proof}
  Semisimple categories can be characterized by vanishing of non-split extensions, i.e. we have 
  \begin{inlem}
    \todocol{Let $\acat$ be an abelian category, such that 
    $ 
      \ext^1_{\acat}(S,S') = 0 
    $
    holds for all simple objects $S,S'\in \acat$. Then $\acat$ is semisimple.}
  \end{inlem}
  By definition (and \cref{prop:simple}), this is the same as showing 
  \[
    \hom_{\detcat*(\grs_G,\qlb)}(\ic_\lambda, \ic_\mu[1]) = 0
  \]
  for all $\lambda,\mu \in X_\ast^\vee$.\footnote{In the original paper, it mostly done for some $\detcat*(\olo_\nu,\qlb)$. This is also fine, because $\rderived i_{\nu,\ast}$ is faithful.}
  We will do this by treating the several possible relations of $\lambda$ and $\mu$ separately.
  The main ingredient (besides the formalism of perverse sheaves) is the following result, which is often called \emph{parity vanishing}:\footnote{We however only need it in one of the three cases.}
  \begin{inlem}[{\cite[11c]{lusztig}}]
    \label{lem:lusztig-parity}
    Let $\mu \in X_+^\vee$, and consider the closed immersion $i \mc \olo_\mu \setminus \oo_\mu \hookrightarrow \olo_\mu$.
    Then $i^\ast \ic_\mu$ is concentrated in even perverse degree.
  \end{inlem}
  \begin{case}
    $\lambda = \mu$.
  \end{case}
  \begin{proof*}
    Consider the following diagram of inclusions:
    \[
      \begin{tikzcd}
        \oo_\mu 
        \ar[hookrightarrow]{r}[above]{j}
        \ar[hookrightarrow]{rd}[below left]{j_\mu}
        &
        \olo_\mu 
        \ar[hookleftarrow]{r}[above]{i}
        \ar[hookrightarrow]{d}[right]{i_\mu} 
        &
        \olo_\mu \setminus \oo_\mu
        \\ 
        &
        \grs_G 
        &
      \end{tikzcd}
    \]
    For brevity, we write $\iota =  i_\mu \circ i$ for the composition.
    By \eqref{eq:pvs-basic}, we have that $\iota^\ast \ic_\mu$ is concentrated in negative perverse degree, and $\iota^! \ic_\mu$ is concentrated in positive perverse degree, and thus 
    \begin{equation}
      \label{eq:ic-shriek-zero}
      \hom_{\detcat*(\olo_\mu \setminus \oo_\mu,\qlb)}(\iota^\ast \ic_\mu, \iota^! \ic_\mu[1]) = 0.
    \end{equation}
    The open-closed decomposition of $\olo_\mu$ now yields for every $A \in \detcat*(\olo_\mu,\qlb)$ a distinguished triangle 
    \[
      \begin{tikzcd}[column sep = small]
        j_! j^\ast A
        \ar{r}
        &
        A
        \ar{r}
        &
        i_\ast i^\ast \ic_\mu 
        \ar{r}
        & 
        {[1]}
      \end{tikzcd}
    \]
    of complexes in $\detcat*(\olo_\mu, \qlb)$. 
    For $A = i_\mu \ic_\mu$ this gives, after applying the cohomological functor $\hom_{\detcat*(\grs_G,\qlb)}(i_{\mu,!}-,\ic_\mu[1])$, the exact sequence of abelian groups\footnote{now dropping the index $\detcat*(\grs_G,\qlb)$ from the notation} 
    \[
      \begin{tikzcd}[column sep = small]
        {\hom(\iota_!\iota^\ast \shf, \ic_\mu[1])}
        \ar{r}
        &
        {\hom(\ic_\mu,\ic_\mu[1])}
        \ar{r}
        &
        {\hom(j_{\mu,!}\csh{\qlb}[\dim \oo_\mu], \ic_\mu[1])}
      \end{tikzcd},
    \]
    and we need to see that the middle term is zero.
    We do this by arguing that both of the outer terms are zero.
    For the left group, this follows from combining \eqref{eq:ic-shriek-zero} and the $(-)_! \dashv (-)^!$ adjunction. 
    For the right group, we have 
    \begin{align*}
      \hom(j_{\lambda,!}\csh{\qlb}[\dim \oo_\mu], \ic_\mu[1]
      &
      \cong 
      \hom_{\detcat*(\oo_\mu, \csh{\qlb})}(\qlb[\dim \oo_\mu], j_\lambda^! \ic_\mu[1])
      \\ 
      &
      \cong 
      \hom_{\detcat*(\oo_\mu,\qlb)}(\csh{\qlb}, \csh{\qlb}[1]),
    \end{align*}
    because by $\vd(\vd(\Lambda)) = \Lambda$, and  
    \[
      j_\mu^! \vd(j_{\mu,!\ast}\shl)
      = 
      \vd(j_\mu^\ast j_{\mu,!\ast}\shl)
      \vd(\shl)[\dim \oo_\mu]
    \]
    holds for any local system $\shl$ on $\oo_\mu$ (this is a general statement about Verdier duality and its interaction with intersection cohomology (i.e. we do not need to assume that $\oo_\mu$ does not admit non-trivial local systems).
    \par 
    Now $\hom_{\detcat*(\oo_\mu,\Lambda)}(\csh{\Lambda},\csh{\Lambda}) = \hh^1(\oo_\mu,\Lambda)$ vanishes, because $\oo_\mu$ is simply connected.
  \end{proof*}
  \begin{case}
    $\lambda \neq \mu$ and either $\lambda \leq \mu$ or $\mu \leq \lambda$.
  \end{case}
  \begin{proof*}
    Assume $\mu \leq \lambda$ (the other case follows by exactly the same argument).
    Let $j_\mu \mc \oo_\mu \hookrightarrow\grs_G $ be the locally closed embedding, and let $\shg \in \detcat*(\grs_G,\Lambda)$ be the cone of the adjunction map 
    \begin{equation}
      \label{eq:case2-g-cone}
      \begin{tikzcd}[column sep = small]
        \ic_\mu 
        \ar{r}
        &
        j_{\mu,\ast}j_\mu^\ast \ic_\mu 
        \cong 
        {j_{\mu,\ast} \csh{\qlb}[\dim \oo_\mu]},
      \end{tikzcd}
    \end{equation}
    where the last isormphisms follows from \eqref{eq:pvs-basic}.
    Now since $j_{\mu}$ is a locally closed immersion, the functor $j_{\mu,\ast}$ is left (perverse) t-exact, and hence $j_{\mu,\ast}\csh{\qlb}[\dim \oo_\mu]$ is concentrated in non-negative perverse degrees, and because the map $\ic_\mu \to \pvsp{\shh}^0(j_{\mu,\ast} \csh{\qlb}[\dim \oo_\mu])$ is a monomorphism in the abelian category $\pvs*(\grs_G)$\footnote{\todocol{In \cite[pg.38]{savior}, they say this is due to the ``classical fact'' \cite[(1.4.22.1)]{bdd}. I don't see how this follows from this formular. But I also think this map is just the inclusion $\im\left(\hhp{0}(j_{\mu,!}\shl) \to \hhp{0}(j_{\mu,!}\shl)\right) \hookrightarrow \hhp{0}(j_{\mu,\ast} \shl)$ for $\shl = \csh{\Lambda}[\dim \oo_\mu]$, which is injective in every abelian category.}} the complex $\shg$ is concentrated in non-negative perverse degrees as well.\footnote{\todocol{Let $\tcat$ be a triangulated category with a $t$-structure and write $\tau\mc \tcat \to \tcat^\heartsuit$ for the truncation functor. Let $f\mc A \to B$ be a map in $\tcat$, and assume $A\in \tcat^\heartsuit$, $B \in \tcat_{\geq 0}$ and that $\tau(f)$ is an injective map in the abelian catgeory $\tcat^\heartsuit$. Then $\cone(f) \in \tcat_{\geq 0}$.}}
    Since $\shg$ completes \eqref{eq:case2-g-cone} to an exact triangle, there is an associated exact sequence of abelian groups
    \[
      \begin{tikzcd}[column sep = small]
        \hom(\ic_\lambda, \shg)
        \ar{r}
        &
        {\hom(\ic_\lambda,\ic_\mu[1])}
        \ar{r}
        &
        {\hom(\ic_\lambda,j_{\mu,\ast} \csh{\qlb}[\dim \oo_\mu +1])}
      \end{tikzcd}.
    \]
    Since $\shg$ is concentrated in non-negative perverse degrees and supported on $\olo_\mu$ (as both $\ic_\mu$ and $j_{\mu,\ast}j_\mu^\ast \ic_\mu$ are), we have (similar to the previous case) that $\hom(\ic_\mu, \shg) = 0$.
    \par
    By construction, $j_\mu^\ast \ic_\lambda$ is concentrated in cohomological degree $-\dim \oo_\mu$.
    On the other hand, by \cref{lem:lusztig-parity}, $j^\ast_\mu \ic_\lambda$ has complex only in degrees of the same parity as $\dim(\oo_\lambda)$.
    Since $\dim(\oo_\lambda) \equiv \dim(\oo_\mu) \mod 2$ (they belong to the same connected component of $\grs_G$)\footnote{And the parity of the dimension of a Schubert variety is constant on each connected component.} we thus have that $j_\mu^\ast \ic_\lambda$ is concentrated in cohomological degrees $\leq - \dim \oo_\mu - 2$, and hence 
    \[
      \hom(\ic_\lambda,j_{\mu,\ast} \csh{\qlb} [\dim \oo_\mu +1])
      = 
      \hom(j_\mu^\ast \ic_\lambda, \csh{\qlb}[\dim \oo_\mu +1])
    \]
    vanishes.
    The construction of $\shg$ as cone induces an exact sequence of abelian groups 
    \[
      \begin{tikzcd}[column sep = small]
        \hom(\ic_\lambda, \shg) 
        \ar{r} 
        & 
        {\hom(\ic_\lambda, \ic_\mu[1])}
        \ar{r}
        & 
        {\hom(\ic_\lambda,j_{\mu,\ast}\csh{\Lambda}[\dim \oo_\mu +1]}
      \end{tikzcd}.
    \]
    Since by assumption $\olo_\mu \sse \olo_\lambda \setminus \oo_\lambda$, and $\shg$ is supported on $\olo_\mu$, the left-hand side vanishes, and by the above considerations, the right-hand does as well.
  \end{proof*}
  \begin{case}
    $\lambda \not \leq \mu$ and $\mu \not \leq \lambda$.
  \end{case}
  \begin{proof*}
    Write $i_\mu \mc \olo_\mu \hookrightarrow \grs_G$ for the inclusion.
    Since $\ic_\mu$ is supported on $\olo_\mu$, we have $i_{\mu,\ast}i_\mu^\ast \ic_\mu = \ic_\mu$, and so 
    \[
      \hom_{\detcat*(\grs_G,\qlb)}(\ic_\lambda,\ic_\mu[1]) 
      \cong 
      \hom_{\detcat*(\olo_\mu,\qlb)}(i_\mu^\ast \ic_\lambda, i_\mu^\ast \ic_\mu).
    \]
    Set $Z = \olo_\mu \cap \olo_\lambda$, and write $f_\mu\mc Z \hookrightarrow \olo_\mu$ for the closed immersion.
    The point now is that $i_\mu^\ast \ic_\lambda$ is supported on $Z$, and so it is of the form $f_{\mu,!}A$ for some complex of sheaves $A\in \detcat*(Z,\qlb)$.
    Consider the associated cartesian diagram 
    \[
      \begin{tikzcd}
        \olo_\lambda \cap \olo_\mu 
        \ar{r}[above]{f_\mu}
        \ar{d}[left]{f_\lambda}
        & 
        \olo_\mu 
        \ar{d}[right]{i_\mu}
        \\ 
        \olo_\lambda 
        \ar{r}[below]{i_\lambda}
        & 
        \grs_G 
      \end{tikzcd}
    \]
    By proper base change, we have $i_\mu^\ast i_{\lambda,\ast} \ic_\lambda = f_{\mu_\ast}f_\lambda^\ast \ic_\lambda$, and so by adjunction 
    \[
      \hom_{\detcat*(\grs_G,\Lambda)}
      (i_{\lambda,\ast}\ic_\lambda, i_{\mu,\ast}\ic_\mu)
      = 
      \hom_{\detcat*(\olo_\mu\cap \olo_\lambda,\Lambda)}(f_\lambda^\ast \ic_\lambda,f_\mu^!\ic_\mu[1]),
    \]
    and the later group vanishes.\footnote{\todocol{Let $Z_1,Z_2\sse X$ be two locally closed subschemes, such that $\overline{Z_1} \subsetneq \overline{Z_2}$ and vice versa. Let $\iota_i\mc \overline{Z_1}\cap \overline{Z_2} \hookrightarrow \overline{Z_i}$ be the two inclusions. Then $\iota_i^\ast \ic_i$ is concentrated in negative perverse degree, and $\iota_i^!\ic_i$ is concentrated in positive perverse degree, for any intersection cohomology sheaf $\ic_i$ on $Z_i$.}}
    Arguing as before, this implies that in fact \[\hom_{\detcat*(\grs_G,\Lambda)}(\ic_\lambda,\ic_\mu[1]) = 0.\]
  \end{proof*}

\end{proof}


\section{Fiber functor on $\pvs*(\grs_G)$}

Let 
\[
  \omega(-) 
  = 
  \bigoplus_{i\in \zz}
  R^i\Gamma(\grs_G,-)
  \mc \pvs*(\grs_G)
  \longrightarrow 
  \vec_{\qlb}
\]
be the cohomology functor with values in finite-dimensional $\qlb$-vector spaces.

\begin{lem}
  \label{lem:omega-pre}
  The functor $\omega \mc \pvs*(\grs_G) \to \vec_{\qlb}$ is additive, exact and faithful.
\end{lem}

\begin{proof}
  Additivity is immediate.
  Moreover, since $\pvs*(\grs_G)$ is semisimple, every short-exact sequence in it splits, and since $\omega$ is additive, it follows that it is exact.
  To show faithfulness, it suffices (in light of \cref{prop:semisimple}) to show that $\omega(\ic_\mu) \neq 0$ for any $\mu \in X_+^\vee$.
  This holds in fact for any projective variety (such as $\olo_\mu$), and follows from the \emph{decomposition theorem} for the proper pushforward of perverse sheaves:
  \begin{inthm}
    Let $f\mc Y \to X$ be a proper morphism of varieties.
    If $C \in \detcat*(Y,\Lambda)$ is a direct sum of (shifted) simple complexes, then so is $Rf_\ast C \in \detcat(Y,\Lambda)$.
  \end{inthm}
  Now each Schubert variety is projective, and the intersection cohomology of the projective space is non-zero.\footnote{Richarz argues that we can find a generically finite morphism $\pi \mc \olo_\mu \to \mathbb{P}^n$, but I do not know why we need to add this.}
\end{proof}

\begin{prop}
  The pair $(\pvs*(\grs_G),\star)$ is a neutral Tannakian category with fiber functor $\omega\mc \pvs*(\grs_G) \to \vec_{\qlb}$.
\end{prop}
\begin{proof}
  We use \cref{prop:tannaka-reconst}.
  In the previous talks we have seen that $\pvs*(\grs_G)$ together with the convolution product $\star$ is a symmetric monoidal category, and that $\omega$ is a symmetric monoidal functor.
  Additivity, exactness and faithfulness of $\omega$ were the content of the previous \cref{lem:omega-pre}.
  So it remains to show that $(\pvs*(\grs_G),\star)$ has a unit object, and that any one dimensional object has an inverse.
  The unit object is given by $\ic_0 \simeq \csh{\qlb}$ concentrated in the base point $e_0$.
  Finally, we note that we have $\dim(\ic_\mu(A)) = 1$ if and only if $\dim \olo_\mu = 0$ (c.f. \cite[Prop.1.5.13]{savior}).
  In that case, $\ic_{-\mu} \star \ic_{\mu} \simeq \ic_0$.\footnote{This holds true always for the convolution product of IC-sheaves supported on singletons.}
\end{proof}

\begin{rem}
  Equiped with this proposition and \cref{thm:reconst}, the missing part for proving the geometric Satake equivalence is the identification 
  \[
    \iaut*(\omega)
    = 
    \ldu{G},
  \]
  where $\ldu{G}$ is the \emph{Langlands dual group} of $G$.
  This is the topic of the next talk.
\end{rem}

%\appendix 
%\section{Open questions that arose while preparing for the talk}
%There were some things were I was hoping to find examples to illustrate that certain conditions etc are necessary.
%I list them here, in the hope to resolve them later on:
%\begin{question}
%  It is claimed that if we work with $\detcat_{b,\mathcal{S}}^c(X,\Lambda)$, i.e. bounded complexes of sheaves that are constructible with respect to a \emph{given} stratification $\mathcal{S}$, then this not closed under the six functors.
%  What are good examples?
%\end{question}
%
\printbibliography


%\listoftheorems[ignoreall,
%show={todo, danger}]
\end{document}
